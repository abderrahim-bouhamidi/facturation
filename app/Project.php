<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;class Project extends Model
{
    use SoftDeletes, Notifiable;
     protected $fillable = [
        'company', 'description', 'start_date', 'end_date', 'in', 'out'
    ];
}