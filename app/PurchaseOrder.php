<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Company;
use App\Models\Client;
use App\Models\Paiementmode;
use Illuminate\Database\Eloquent\SoftDeletes;
class PurchaseOrder extends Model
{
    use SoftDeletes, Notifiable;
     protected $fillable = [
        'date', 'name', 'document', 'reference', 'clients_id', 'companies_id', 'paiementmodes_id', 'devise', 'tva','total_ht','total_ttc'
    ];
     public function client()
    {
        return $this->belongsTo(Client::class,'clients_id');
    }
    public function paiementmode()
    {
        return $this->belongsTo(Paiementmode::class,'paiementmodes_id');
    }
     public static function newBonCommande()
    {
        $purchaseOrder = new PurchaseOrder();
        $lastOne = Self::orderBy("created_at","DESC")->first();
        
        $purchaseOrder['name'] = "C".'-'.str_pad($lastOne!=null?$lastOne->id+1:1, 6, "0", STR_PAD_LEFT);    
        return $purchaseOrder['name'];
    }
    public static function generate_random(){
        return 1;
    }
    public static function boot() {
        parent::boot();
        static::created(function($client) {
        });
        static::updated(function($client) {
        });
        static::deleting(function($client) {
            $this->client()->delete();
            $this->PurchaseOrder()->delete();
            $this->devis()->delete();
            $this->lines()->delete();
        });
    }
}