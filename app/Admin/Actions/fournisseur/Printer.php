<?php

namespace App\Admin\Actions\fournisseur;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;

class Printer extends RowAction
{
    public $name = 'print';
    // public function href()
    // {
    //     return $this->getResource()."/".$this->getKey()."/print";
    // }

    public function render()
    {

        return '<a target="_blank" href="'.$this->getResource().'/'.$this->getKey().'/print-it">print</a>';
    }

}