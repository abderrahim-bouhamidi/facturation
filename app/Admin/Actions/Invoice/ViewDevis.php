<?php

namespace App\Admin\Actions\Invoice;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use App\Models\Devis;

class ViewDevis extends RowAction
{
    public $name = 'view';
    public function render()
    {
        return '<a target="_blank" href="'.$this->getResource().'/'.$this->getKey().'/view-devis">view Devis</a>';
    }

}