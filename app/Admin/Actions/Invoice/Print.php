<?php

namespace App\Admin\Actions\Invoice;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;

class Print extends RowAction
{
    public $name = 'print';

    public function handle(Model $model)
    {
        // $model ...

        return $this->response()->success('Success message.')->refresh();
    }

}