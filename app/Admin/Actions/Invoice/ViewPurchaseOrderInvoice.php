<?php

namespace App\Admin\Actions\Invoice;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use App\Models\Invoice;
class ViewPurchaseOrderInvoice extends RowAction
{
    public $name = 'view';

    // public function href()
    // {
    //     return $this->getResource()."/".$this->getKey()."/print";
    // }

    public function render()
    {
        return '<a target="_blank" href="'.$this->getResource().'/'.$this->getKey().'/view-invoice-bc">view BC</a>';
    }

}