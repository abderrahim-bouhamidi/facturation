<?php

namespace App\Admin\Actions\Invoice;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;

class PrintInvoice extends RowAction
{
    public $name = 'print';
    // public function href()
    // {
    //     return $this->getResource()."/".$this->getKey()."/print";
    // }

    public function render()
    {

        return '<a target="_blank" href="'.$this->getResource().'/'.$this->getKey().'/print">print</a>';
    }

}