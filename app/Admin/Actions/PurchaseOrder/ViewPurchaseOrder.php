<?php

namespace App\Admin\Actions\PurchaseOrder;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use App\PurchaseOrder;
class ViewPurchaseOrder extends RowAction
{
    public $name = 'view';

    // public function href()
    // {
    //     return $this->getResource()."/".$this->getKey()."/print";
    // }

    public function render()
    {
        return '<a target="_blank" href="'.$this->getResource().'/'.$this->getKey().'/view">view</a>';
    }

}