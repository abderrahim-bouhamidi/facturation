<?php

namespace App\Admin\Actions\Devis;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use App\Models\Devis;
class ViewPurchaseCommand extends RowAction
{
    public $name = 'view';

    // public function href()
    // {
    //     return $this->getResource()."/".$this->getKey()."/print";
    // }

    public function render()
    {
        return '<a target="_blank" href="'.$this->getResource().'/'.$this->getKey().'/view-bc">view BC</a>';
    }

}