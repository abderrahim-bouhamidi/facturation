<?php

namespace App\Admin\Actions\client;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use App\Models\Devis;

class ViewQuotation extends RowAction
{
    public $name = 'view';
    public function render()
    {
        return '<a target="_blank" href="'.$this->getResource().'/'.$this->getKey().'/view-quotation-client">view Devis</a>';
    }

}