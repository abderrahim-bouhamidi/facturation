<?php

namespace App\Admin\Actions\client;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use App\PurchaseOrder;

class ViewPurchaseOrder extends RowAction
{
    public $name = 'view';
    public function render()
    {
        return '<a target="_blank" href="'.$this->getResource().'/'.$this->getKey().'/view-proposal-client">view proposal</a>';
    }

}