<?php

namespace App\Admin\Actions\client;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use App\Models\Invoice;

class ViewInvoice extends RowAction
{
    public $name = 'view';
    public function render()
    {
            return '<a  href="'.$this->getResource().'/'.$this->getKey().'/view-invoice-client">view Invoice</a>';

    }

}