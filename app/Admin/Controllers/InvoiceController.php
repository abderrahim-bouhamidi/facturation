<?php

namespace App\Admin\Controllers;

use App\Models\Invoice;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
Use Encore\Admin\Widgets\Table;
use Illuminate\Support\Facades\Storage;

use App\Models\Client;
use App\Models\Paiementmode;
use App\Company;
use App\Models\Devis;
use App\PurchaseOrder;

use App\Admin\Actions\Invoice\PrintInvoice;
use App\Admin\Actions\Invoice\ViewPurchaseOrderInvoice;
use App\Admin\Actions\Invoice\ViewDevis;
use Request;
use App\Admin\Actions\invoice\ViewInvoice;

use App\Chiffres;
use PDF;
Use \Carbon\Carbon;

class InvoiceController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    static public $var;
    protected $title = 'Factures';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Invoice());
         if (\Request::is('admin/client/invoices')) {
            $grid->model()
                ->where('contre_partie', 'client')
                ->groupBy('invoices.id');
                $var=1;
        }
        if(\Request::is('admin/fournisseur/invoices')){
            $grid->model()
                ->where('contre_partie', 'fournisseur')
                ->groupBy('invoices.id');
                $var=2;
        }
        $grid->column('id', __('Id'));
        $grid->column('name', __('Name'));
        $grid->column('date', __('Date'));
        if (\Request::is('admin/client/invoices')) {

        $grid->column('reference', __('Reference'))->expand(function ($model) {
            $lignes = $model->lines->map(function ($line) {
                return $line->only(['id', 'reference', 'description', 'qty', 'price_uht']);
            });
            return new Table(['ID', 'reference', 'description', 'qty', 'price_uht'], $lignes->toArray());
        });
    }
        if(\Request::is('admin/client/invoices')){
            $grid->column('clients_id', __('Clients'))->display(function($clients_id) {
                return Client::find($clients_id)->company;
            });
            $grid->column('companies_id', __('Companies'))->display(function($companies_id) {
               return Company::find($companies_id)->company;
           });
        }
        $grid->column('paiementmodes_id', __('mode de paiement'))->display(function($paiementmodes_id) {
            return Paiementmode::find($paiementmodes_id)->name;
        });      
        $grid->column('devis_id', __('Devis'))->display(function($devis_id) {
            return Devis::find($devis_id)->name;
        });
        $grid->column('purchase_orders_id', __('Bon de commande'))->display(function($purchase_orders_id) {
            return PurchaseOrder::find($purchase_orders_id)->name;
        });
        $grid->column('tva', __('Tva'))->display(function ($tva) { return ($tva*100)."%"; });
        $grid->column('devise', __('Devise'));
        $grid->column('total_ht', __('Total ht'))->display(function ($totatl) { return number_format($totatl,2,"."," "); });
        $grid->column('total_ttc', __('Total ttc'))->display(function ($totatl) { return number_format($totatl,2,"."," "); });
        $grid->column('payement_statuts', __('Statut de paiement'))->editable('select', [
                1 => 'Payé',
                2 => 'Partielement payé',
                3 => 'Non payé',
        ])->label([
            1 => 'success',
            2 => 'warning',
            3 => 'danger',
        ])->style('font-size:14px;text-underline:none;');

        $grid->column('due_date', __('Date d&#39;écheance'))
            ->sortable()
            ->date('Y-m-d')
            ->filter('range', 'date')
            ->display(function ($title, $column) {
                    if($this->due_date < Carbon::now()){
                        return  $column->view('background');
                    }
                    else{
                        return $column->view('other');     
                    }        
            });

        $grid->actions(function ($actions) {
            if(\Request::is('admin/client/invoices')){
                $actions->add(new PrintInvoice);
                $actions->add(new ViewPurchaseOrderInvoice);
                $actions->add(new ViewDevis);
            }
            if(\Request::is('admin/fournisseur/invoices')){
                $actions->add(new ViewInvoice);
                $actions->add(new ViewPurchaseOrderInvoice);
                $actions->add(new ViewDevis);
            }
            
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Invoice::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('date', __('Date'))->as(function ($date) { return date("d/m/Y", strtotime($date)); });
        $show->field('reference', __('Reference'));
        $show->field('client', __('Clients id'))->as(function($client){
            return $client->full_name;
        });
        $show->field('company', __('companies_id'))->as(function($company){
            return $company->company;
        });
        $show->field('paiementmode', __('Paiementmodes id'))->as(function($paiement){
            return $paiement->name;
        });
        $show->field('devis_id', __('Devis id'))->as(function($devis){
            return $devis->name;
        });
        $show->field('purchase_orders_id', __('PurchaseOrder id'))->as(function($purchaseOrder){
            return $purchaseOrder->name;
        });
        $show->field('tva', __('Tva'))->as(function ($tva) { return ($tva*100)."%"; });
        $show->field('total_ht', __('Total ht'))->as(function ($totatl) { return number_format($totatl,2,"."," "); });
        $show->field('due_date', __('Date d&#39;écheance'))->as(function ($date) { return date("d/m/Y", strtotime($date)); });
        $show->field('total_ttc', __('Total ttc'))->as(function ($totatl) { return number_format($totatl,2,"."," "); });
        $show->field('created_at', __('Created at'))->as(function ($date) { return date("d/m/Y H:i:s", strtotime($date)); });
        $show->field('updated_at', __('Updated at'))->as(function ($date) { return date("d/m/Y H:i:s", strtotime($date)); });
        
        $show->lines('Produits', function ($lines) {
            $lines->id();
            $lines->reference();
            $lines->qty();
            $lines->price_uht()->display(function ($totatl) { return number_format($totatl,2,"."," "); });
            
            $lines->disableActions();
            $lines->disablePagination();
            $lines->disableCreateButton();
            $lines->disableFilter();
            $lines->disableRowSelector();
            $lines->disableColumnSelector();
            $lines->disableTools();
            $lines->disableExport();
        });
        $show->field('payement_statuts', __('payement_statuts'));
        
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Invoice());

        $form->date('date', __('Date'))->default(date('Y-m-d'));
        $form->text('name', __('Name'))->default(Invoice::newInvoice());
        $form->text('reference', __('Référence'))->default(Invoice::newReference());
        $form->select('paiementmodes_id', __('Mode de paiement'))->options(Paiementmode::all()->pluck('name', 'id'));
        $form->select('purchase_orders_id', __('Bon de commande'))->options(PurchaseOrder::where('contre_partie', 'client')->pluck('name', 'id'));
        $form->select('devis_id', __('Devis'))->options(Devis::where('contre_partie', 'client')->pluck('name', 'id'));
        $form->text('devise', __('Devise'))->default("MAD");
        $form->decimal('tva', __('Tva'))->default(0.2);
        $form->select('payement_statuts', __('payement_statuts'))->options([
            1 => 'Payé',
            2 => 'Partielement payé',
            3 => 'Non payé',
        ]);
            $form->hasMany('lines','Produits', function (Form\NestedForm $form) {
                $form->text('reference','Référence');
                $form->textarea('description','Description');
                $form->text('qty', 'Quantité');
                $form->text('price_uht','Prix UHT');
            });
        $form->file('document', __('Document'));
        $form->date('due_date', __('Date d&#39;écheance'))->default(date('Y-m-d'));
        $form->select('clients_id', __('Client'))->options(Client::all()->pluck('company', 'id'))->default(1);
       $form->select('companies_id', __('Company'))->options(
                Company::where('contre_partie', 'company_interne')->pluck('company', 'id')
       )->default(1);

        $form->saved(function (Form $form) {
            $invoice = $form->model();
            
            $total = 0;
            foreach ($invoice->lines as $line) {
                $total += $line->qty * $line->price_uht;
            }

            $invoice->total_ht = $total;
            $invoice->total_ttc = $total * (1 + $invoice->tva);
            if (\Request::is('admin/client/invoices')) { 
                $invoice->contre_partie='client';
                $invoice->total_ht = $total;
                $invoice->total_ttc = $total * (1 + $invoice->tva);
            }
            if(\Request::is('admin/fournisseur/invoices')){
                $invoice->contre_partie='fournisseur';
            }
            $invoice->save();
        });

        return $form;
    }

    public function print($id)
    {
        $invoice = Invoice::find($id);
        $chiffres = new Chiffres(round($invoice->total_ttc,2), $invoice->devise);
        $chiffre = $chiffres->convert("fr-FR");
        // return view("invoice.print", compact('invoice','chiffre'));

        $pdf = PDF::loadView("invoice.print", compact('invoice','chiffre'));
        $pdf->save('uploads/invoices/'.$invoice['name'].'.pdf');
        return $pdf->stream();
        return $pdf->download($invoice->reference.'.pdf');
    }
    public function view_facture($id)
    {
        $file_path = Invoice::find($id);
        $file= $file_path->document;
        return view('invoice.view_facture', compact('file'));
    }
    public function preview($id)
    {
        $file_path = Invoice::find($id);
        $file= $file_path->PurchaseOrder->document;
        return view('invoice.preview', compact('file'));
    }
    public function preview_devis($id)
    {   
        $file_path = Invoice::find($id);
        $file = $file_path->Devis->name;
        return view('invoice.preview_devis', compact('file'));
     }
}