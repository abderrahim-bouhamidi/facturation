<?php

namespace App\Admin\Controllers;

use App\Models\Client;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

use App\Admin\Actions\client\ViewInvoice;
use App\Admin\Actions\client\ViewPurchaseOrder;
use App\Admin\Actions\client\ViewQuotation;
use App\Admin\Layout\Content;
use App\Models\Invoice;
use App\Models\Devis;
use App\PurchaseOrder;

class ClientController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Client';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Client());

        $grid->column('id', __('Id'));
        $grid->column('logo', __('Logo'))->image(env('APP_URL').'/uploads', 30,30);
        $grid->column('contact_name', __('Contact name'));
        $grid->column('company', __('Company'));
        $grid->column('email', __('Email'));
        $grid->column('phone', __('Phone'));
        $grid->column('ice', __('ICE'));
        $grid->column('purchase_orders_id', __('Bon_commande'))->display(function($purchase_orders_id) {
            if( $purchase_orders_id === null)
                return 'Aucune bon de commande';
            else
               return  PurchaseOrder::find($purchase_orders_id)->name;
        });
         $grid->column('devis_id', __('Devis'))->display(function($devis_id) {
             if( $devis_id === null)
                return 'Aucune Devis';
            else
               return  Devis::find($devis_id)->document;
        });
        $grid->column('invoices_id', __('Facture'))->display(function($invoices_id) {
            if( $invoices_id === null)
                return 'Aucune Facture';
            else
                return Invoice::find($invoices_id)->name;
        });       


        $grid->actions(function ($actions) {
            $actions->add(new ViewInvoice);
            $actions->add(new ViewPurchaseOrder);
            $actions->add(new ViewQuotation);
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Client::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('logo', __('Logo'))->image();
        $show->field('contact_name', __('Contact name'));
        $show->field('company', __('Company'));
        $show->field('email', __('Email'));
        $show->field('phone', __('Phone'));
        $show->field('ice', __('ICE'));
        $show->field('address', __('Address'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('purchase_orders_id', __('Bon_commande'))->as(function($purchaseOrder){
            return $purchaseOrder->name;
        });
        $show->field('devis_id', __('Devis id'))->as(function($devis){
            return $devis->name;
        });
        $show->field('invoices_id', __('FACTURE '))->as(function($invoice){
            return $invoice->name;
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Client());
        $form->image('logo', __('Logo'))->uniqueName();
        $form->text('contact_name', __('Contact name'))->required();
        $form->text('company', __('Company'))->required();
        $form->email('email', __('Email'));
        $form->mobile('phone', __('Phone'));
        $form->text('ice', __('ICE'));
        $form->textarea('address', __('Address'));
        $form->select('purchase_orders_id', __('Bon de commande'))->options(PurchaseOrder::all()->pluck('name', 'id'));
        $form->select('devis_id', __('Devis'))->options(Devis::all()->pluck('name', 'id'));
        $form->select('invoices_id', __('Facture'))->options(Invoice::all()->pluck('name', 'id'));
        return $form;
    }
    public function view_proposal($id)
    {   
        $file_path = Client::find($id);
        if($file_path->purchase_orders_id != null){
            $file = $file_path->PurchaseOrder->document;
            return view('client.preview_proposal', compact('file'));
        }
        else
         return admin_info('Note','Aucune Bon de commande');    

    }
    public function view_quote($id)
    {   
        $file_path = Client::find($id);
        if($file_path->devis_id == 'Aucune Devis'){
         $file = $file_path->Devis->document;
         return view('client.preview_quotation', compact('file'));
         }
        else
         return admin_info('Note','Aucune Devis');
 
    }
    public function view_invoice($id)
    {
        $file_path = Client::find($id);
        if($file_path->invoices_id != null){
            $file= $file_path->Invoice->name;
            return view('client.preview_invoice', compact('file'));
        }
        else 
         return admin_info('Note','Aucune Facture');
    }
}