<?php

namespace App\Admin\Controllers;

use App\Models\Devis;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
Use Encore\Admin\Widgets\Table;

use App\Models\Client;
use App\Company;
use App\PurchaseOrder;
use App\Models\Paiementmode;

use App\Admin\Actions\Invoice\PrintInvoice;
use App\Admin\Actions\fournisseur\Printer;
use App\Admin\Actions\Devis\ViewPurchaseCommand;
use Request;
use App\Chiffres;
use PDF;


class DevisController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    static protected $var;
    protected $title = 'Devis';
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {   
        $grid = new Grid(new Devis());
       if (\Request::is('admin/client/devis')) {
            $grid->model()
                ->where('contre_partie', 'client')
                ->groupBy('devis.id');
                $var=1;
        }
        if(\Request::is('admin/fournisseur/purchase-orders')){
            $grid->model()
                ->select('id','name','date','Reference','companies_id','paiementmodes_id','devise','tva','total_ht','total_ttc','document','contre_partie','updated_at','created_at')
                ->where('contre_partie', 'fournisseur')
                ->groupBy('devis.id');
                $var=2;
        }
        $grid->column('id', __('Id'));
        $grid->column('name', __('Name'));
        $grid->column('date', __('Date'));
        $grid->column('reference', __('Reference'))->expand(function ($model) {
            $lignes = $model->lines->map(function ($line) {
                return $line->only(['id', 'reference', 'description', 'qty', 'price_uht']);
            });
            return new Table(['ID', 'reference', 'description', 'qty', 'price_uht'], $lignes->toArray());
        });

        if($var===1){
            $grid->column('clients_id', __('Clients'))->display(function($clients_id) {
                return Client::find($clients_id)->company;
            });       
        }
        $grid->column('companies_id', __('Companies'))->display(function($companies_id) {
            return Company::find($companies_id)->company;
        });
        $grid->column('paiementmodes_id', __('mode de paiement'))->display(function($paiementmodes_id) {
            return Paiementmode::find($paiementmodes_id)->name;
        });
        if (\Request::is('admin/client/devis')) {
        $grid->column('purchase_orders_id', __('Bon de commande'))->display(function($purchase_orders_id) {
            return PurchaseOrder::find($purchase_orders_id)->name;
        });
        }
        $grid->column('tva', __('Tva'))->display(function ($tva) { return ($tva*100)."%"; });
        $grid->column('total_ht', __('Total ht'))->display(function ($totatl) { return number_format($totatl,2,"."," "); });
        $grid->column('total_ttc', __('Total ttc'))->display(function ($totatl) { return number_format($totatl,2,"."," "); });

        $grid->actions(function ($actions) {
                if (\Request::is('admin/client/devis')) {
                    $var=1;    
                }
                if(\Request::is('admin/fournisseur/purchase-orders')){
                    $var=2;
                }
                if($var===1){
                    $actions->add(new PrintInvoice);
                    $actions->add(new ViewPurchaseCommand); 
                }
                if($var===2){
                    $actions->add(new Printer);
                }
        });
        $grid->column('document', __('Document'));
        $grid->column('contre_partie', __('Contre Partie'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Devis::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('date', __('Date'))->as(function ($date) { return date("d/m/Y", strtotime($date)); });
        $show->field('reference', __('Reference'));
        $show->field('client', __('Clients id'))->as(function($client){
            return $client->full_name;
        });
        $show->field('company', __('Companies id'))->as(function($company){
            return $company->company;
        });
        $show->field('paiementmodes_id', __('mode de paiement'))->display(function($paiementmodes_id) {
            return Paiementmode::find($paiementmodes_id)->name;
        });
        $show->field('purchase_orders_id', __('Bon de commande'))->display(function($purchase_orders_id) {
            return PurchaseOrder::find($purchase_orders_id)->name;
        });
        $show->field('tva', __('Tva'))->as(function ($tva) { return ($tva*100)."%"; });
        $show->field('total_ht', __('Total ht'))->as(function ($totatl) { return number_format($totatl,2,"."," "); });
        $show->field('total_ttc', __('Total ttc'))->as(function ($totatl) { return number_format($totatl,2,"."," "); });
        $show->field('created_at', __('Created at'))->as(function ($date) { return date("d/m/Y H:i:s", strtotime($date)); });
        $show->field('updated_at', __('Updated at'))->as(function ($date) { return date("d/m/Y H:i:s", strtotime($date)); });

        $show->lines('Produits', function ($lines) {
            $lines->id();
            $lines->reference();
            $lines->qty();
            $lines->price_uht()->display(function ($totatl) { return number_format($totatl,2,"."," "); });

            $lines->disableActions();
            $lines->disablePagination();
            $lines->disableCreateButton();
            $lines->disableFilter();
            $lines->disableRowSelector();
            $lines->disableColumnSelector();
            $lines->disableTools();
            $lines->disableExport();
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Devis());

        $form->date('date', __('Date'))->default(date('Y-m-d'));
        $form->text('name', __('Name'))->default(Devis::newDevis());
        $form->text('reference', __('Référence'))->default(Devis::newReference());
            $form->select('companies_id', __('Company'))->options(\App\Company::where('contre_partie', 'company_interne')->pluck('company', 'id'));
            $form->select('clients_id', __('Client'))->options(\App\Models\Client::all()->pluck('full_name', 'id'));
            $form->select('purchase_orders_id', __('Bon de commande'))->options(PurchaseOrder::all()->pluck('name', 'id'));
        
        $form->select('paiementmodes_id', __('Mode de paiement'))->options(Paiementmode::all()->pluck('name', 'id'));
        $form->text('devise', __('Devise'))->default("MAD");
        $form->decimal('tva', __('Tva'))->default(0.2);

        $form->hasMany('lines','Produits', function (Form\NestedForm $form) {
            $form->text('reference','Référence');
            $form->textarea('description','Description');
            $form->text('qty', 'Quantité');
            $form->text('price_uht','Prix UHT');
        });

        $form->saved(function (Form $form) {
            $devis = $form->model();
            
            $total = 0;
            foreach ($devis->lines as $line) {
                $total += $line->qty * $line->price_uht;
            }
            $devis->total_ht = $total;
            $devis->total_ttc = $total * (1 + $devis->tva);
            $devis->document = 'uploads/devis/'.$devis['name'].'.pdf';
            if (\Request::is('admin/client/devis')) { 
                $devis->contre_partie='client';
            }
            if(\Request::is('admin/fournisseur/purchase-orders')){
                $devis->contre_partie='fournisseur';
            }
            $devis->save();
        });

        return $form;
    }

    public function print($id)
    {
        $devis = Devis::find($id);
        $chiffres = new Chiffres(round($devis->total_ttc,2), $devis->devise);
        $chiffre = $chiffres->convert("fr-FR");
        $pdf = PDF::loadView("devis.print", compact('devis','chiffre'));

        $pdf->save('uploads/devis/'.$devis['name'].'.pdf');
        return $pdf->stream();
        return $pdf->download($devis->reference.'.pdf');
    }
    public function printer($id)
    {
        $devis = Devis::find($id);
        $chiffres = new Chiffres(round($devis->total_ttc,2), $devis->devise);
        $chiffre = $chiffres->convert("fr-FR");
        $pdf = PDF::loadView("devis.print", compact('devis','chiffre'));

        $pdf->save('uploads/devis/'.$devis['name'].'.pdf');
        return $pdf->stream();
        return $pdf->download($devis->reference.'.pdf');
    }
    public function view_bc($id)
    {
        $file_path = Devis::find($id);
        $file= $file_path->PurchaseOrder->document;
        return view('devis.preview', compact('file'));
    }
}