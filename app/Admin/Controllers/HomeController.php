<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;

use Encore\Admin\Widgets\InfoBox;

use App\Models\Client;
use App\Models\Devis;
use App\Models\Invoice;
use App\Company;
use App\PurchaseOrder;
use DB;
class HomeController extends Controller
{
     public static function Client()
    {   
        $client_count = Client::all()->count();
        return view('dashboard.client',compact('client_count'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function Company()
    {
        $company_count = Company::all()->where('contre_partie', 'fournisseur')->groupBy('companies.id')->count();
        return view('dashboard.company',compact('company_count'));
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function Devis()
    {
        $devis_count = Devis::all()->where('contre_partie', 'client')->groupBy('devis.id')->count();
        return view('dashboard.devis',compact('devis_count'));
    }

    public static function Invoice()
    {
        $invoices_count = Invoice::all()->count();
        return view('dashboard.invoice',compact('invoices_count'));
    }

     public static function Proposals()
    {
        $proposals_count = PurchaseOrder::all()->count();
        return view('dashboard.proposals',compact('proposals_count'));
    }
    
    public static function Show(){
        // $a = DB::table('invoices as in')
        //     ->select('in.*')
        //     ->selectRaw('SUM(total_ht) AS income')
        //     ->selectRaw('MONTH(date) AS month')     
        //     ->where('contre_partie', '=', 'client')
        //     ->groupBy('month');
        // // dd($a->get()->toArray());

        // $b = DB::table('invoices as out')
        //     ->select('out.*')
        //     ->selectRaw('SUM(total_ht) AS outcome')
        //     ->selectRaw('MONTH(date) AS month')        
        //     ->where('contre_partie', '=', 'fournisseur')
        //     ->groupBy('month')->get()->toArray(); 

        // dd($b->get()->toArray());

        // $z = DB::table('invoices as inout')->select('in.*','out.*')
        //         ->selectRaw('MONTH(date) AS month')     
        //         ->unionAll($a)
        //         ->unionAll($b)
        //         ->get()->toArray();

        $res = DB::table('invoices')->groupBy(['month','contre_partie'])->select('*')
                ->selectRaw('MONTH(date) AS month')  
                ->selectRaw('SUM(total_ht) AS outcome')
                ->selectRaw('SUM(total_ht) AS income')
                ->selectRaw(DB::raw('(CASE WHEN contre_partie = "fournisseur" THEN SUM(total_ht) ELSE 0 END) AS outcome'))
                ->selectRaw(DB::raw('(CASE WHEN contre_partie = "client" THEN SUM(total_ht) ELSE 0 END) AS income'))->get()->toArray();      
        $viewer = $res;
        // dd($viewer);
        // dd($var);    
         $click = DB::table('invoices')->groupBy('year','contre_partie')->select('*')
                ->selectRaw('YEAR(date) AS year')  
                ->selectRaw('SUM(total_ht) AS outcome')
                ->selectRaw('SUM(total_ht) AS income')
                ->selectRaw(DB::raw('(CASE WHEN contre_partie = "fournisseur" THEN SUM(total_ht) ELSE 0 END) AS outcome'))
                ->selectRaw(DB::raw('(CASE WHEN contre_partie = "client" THEN SUM(total_ht) ELSE 0 END) AS income'))
                ->groupBy('year','contre_partie')->get()->toArray();      
        
        foreach($viewer as $key => $elem)
        {
            $viewer[$key]->month = self::month_name($viewer[$key]->month);
        }
        return view('admin.charts.time_chart',compact('viewer')); 
            //     ->with('viewer',json_encode($viewer,JSON_NUMERIC_CHECK))
            //     ->with('click',json_encode($click,JSON_NUMERIC_CHECK))
            //    ;
    }   
    public static function filterDate_income_outcome(){   
       $viewer = Invoice::groupBy('month')->select('*') 
                            ->selectRaw('MONTH(date) month')
                            ->selectSub(
                                Invoice::selectRaw('SUM(total_ht)')
                                       ->where('contre_partie','=', 'client')
                            ,'income')
                            ->selectSub(
                                Invoice::selectRaw('SUM(total_ht)')
                                    ->where('contre_partie','=', 'fournisseur'),
                            'outcome')
                            ->get()
                            ->toArray();
        $click = Invoice::groupBy('year')->select('*') 
                            ->selectSub(
                                Invoice::selectRaw('SUM(total_ht)')
                                       ->where('contre_partie','=', 'client')
                            ,'income')
                            ->selectSub(
                                Invoice::selectRaw('SUM(total_ht)')
                                    ->where('contre_partie','=', 'fournisseur'),
                            'outcome')
                            
                            ->selectRaw('YEAR(date) year')
                            ->get();

        return view('admin.charts.time') 
                ->with('viewer',json_encode($viewer,JSON_NUMERIC_CHECK))
                ->with('click',json_encode($click,JSON_NUMERIC_CHECK));
    }   
     public static function filter_table(){   
        $click = Invoice::groupBy('year')->select('*') 
                            ->selectSub(
                                Invoice::selectRaw('SUM(total_ht)')
                                       ->where('contre_partie','=', 'client')
                            ,'income')
                            ->selectSub(
                                Invoice::selectRaw('SUM(total_ht)')
                                    ->where('contre_partie','=', 'fournisseur'),
                            'outcome')
                            
                            ->selectRaw('YEAR(date) year')
                            ->get();

        return view('admin.charts.table') 
                ->with('click',json_encode($click,JSON_NUMERIC_CHECK));
    }   
    
    public function index(Content $content)
    {
        $content->row(function (Row $row) {

                $row->column(4, function (Column $column) {
                    $column->append(self::Client());
                });

                $row->column(4, function (Column $column) {
                    $column->append(self::Company());
                });

                $row->column(4, function (Column $column) {
                    $column->append(self::Invoice());
                });
            });
        $content->row(function (Row $row) {
                $row->column(12, function (Column $column) {
                    $column->append(self::Show());
                });
        });
        $content->row(function (Row $row) {
                $row->column(12, function (Column $column) {
                    $column->append(self::filterDate_income_outcome());
                });
        });
        
        return $content;
    }

    public static function month_name($var){
        switch ($var) {
            case '1':
                return 'Jan';
                break;
            case '2':
                return 'Feb';
                break;
            case '3':
                return 'Mar';
                break;
            case '4':
                return 'Apr';
                break;
            case '5':
                return 'May';
                break;
            case '6':
                return 'Jun';
                break;
            case '7':
                return 'Jul';
                break;
            case '8':
                return 'Aug';
                break;
            case '9':
                return 'Sep';
                break;
            case '10':
                return 'Oct';
                break;
            case '11':
                return 'Nov';
                break;
            case '12':
                return 'Dec';
                break;
            
            default:
                break;
        } 
    }
   
}