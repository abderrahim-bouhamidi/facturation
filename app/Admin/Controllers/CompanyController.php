<?php

namespace App\Admin\Controllers;

use App\Company;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
Use Encore\Admin\Widgets\Table;

use Request;
class CompanyController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */

    protected $title = 'Company';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Company());
        if (\Request::is('admin/companies')) {
            $grid->model()
                ->where('contre_partie', 'company_interne')
                ->groupBy('companies.id');
        }
        if(\Request::is('admin/fournisseur/fournisseur')){
            $grid->model()
                ->where('contre_partie', 'fournisseur')
                ->groupBy('companies.id');
        }
        $grid->column('id', __('Id'));
        $grid->column('logo', __('Logo'))->image(env('APP_URL').'/uploads', 50,50);
        $grid->column('company', __('Company'));
        $grid->column('address', __('Address'));
        $grid->column('email', __('Email'));
        $grid->column('phone', __('Phone'));
        $grid->column('website', __('Website'));
        $grid->column('ICE', __('ICE'));
        $grid->column('FISC', __('FISC'));
        $grid->column('RC', __('RC'));
        $grid->column('TP', __('TP'));
        $grid->column('CNSS', __('CNSS'));
        $grid->column('RIB', __('RIB'));
        $grid->column('tampon', __('Tampon'))->image(env('APP_URL').'/uploads', 50,50);;
        $grid->column('contre_partie', __('Contre Partie'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Company::findOrFail($id));

        $show->field('id', __('Id'));
        $show->image('logo', __('Logo'));
        $show->field('address', __('Address'));
        $show->field('company', __('Company'));
        $show->field('email', __('Email'));
        $show->field('phone', __('Email'));
        $show->field('website', __('Website'));
        $show->field('ICE', __('ICE'));
        $show->field('FISC', __('FISC'));
        $show->field('RC', __('RC'));
        $show->field('TP', __('TP'));
        $show->field('CNSS', __('CNSS'));
        $show->field('RIB', __('RIB'));
        $show->image('tampon', __('Tampon'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Company());

        $form->image('logo', __('Logo'))->uniqueName();
        $form->textarea('address', __('Address'));
        $form->text('company', __('Company'));
        $form->email('email', __('Email'));
        $form->text('phone', __('Phone'));
        $form->text('website', __('Website'));
        $form->text('ICE', __('ICE'));
        $form->text('FISC', __('FISC'));
        $form->text('RC', __('RC'));
        $form->text('TP', __('TP'));
        $form->text('CNSS', __('CNSS'));
        $form->text('RIB', __('RIB'));
        $form->image('tampon', __('Tampon'))->uniqueName();
        $form->saved(function (Form $form) {
            $company = $form->model();
            if (\Request::is('admin/companies')) { 
                $company->contre_partie='company_interne';
            }
            if(\Request::is('admin/fournisseur/fournisseur')){
                $company->contre_partie='fournisseur';
            }
            $company->save();

         });
        return $form;
    }
}
