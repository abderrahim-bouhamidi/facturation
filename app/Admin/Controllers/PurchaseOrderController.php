<?php

namespace App\Admin\Controllers;

use App\PurchaseOrder;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
Use Encore\Admin\Widgets\Table;
use Illuminate\Support\Facades\Storage;

use App\Models\Client;
use App\Models\Paiementmode;
use App\Company;
use App\Admin\Actions\PurchaseOrder\ViewPurchaseOrder;

class PurchaseOrderController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
        protected $title;


        static public $all;

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new PurchaseOrder());

        if (\Request::is('admin/client/purchase-orders')) { 
            $grid->model()
                ->select('id','name','date','clients_id','companies_id','paiementmodes_id','devise','tva','total_ht','total_ttc','created_at','updated_at')
                ->where('contre_partie', 'client')  
                ->groupBy('purchase_orders.id');
            $all = 1;
            $title = 'Bon de commande';
        }
       if(\Request::is('admin/fournisseur/devis')){
            $grid->model()
                 ->select('id','name','date','document','companies_id','paiementmodes_id','devise','tva','total_ht','total_ttc','created_at','updated_at')
                ->where('contre_partie', 'fournisseur')
                ->groupBy('purchase_orders.id');
            $all= 2;
        }

        $grid->column('id', __('Id'));
        $grid->column('name', __('Name'));
        $grid->column('date', __('Date'));
        // $grid->column('document', __('Document'));
        if($all===1){
           $grid->column('clients_id', __('Clients'))->display(function($clients_id) {
                        return Client::find($clients_id)->company;
            });
        }

        // $grid->column('companies_id', __('Companies'))->display(function($companies_id) {
        //             return Company::find($companies_id)->company;
        // });

        $grid->column('paiementmodes_id', __('mode de paiement'))->display(function($paiementmodes_id) {
            return Paiementmode::find($paiementmodes_id)->name;
        });

        $grid->column('devise', __('Devise'));
        $grid->column('tva', __('Tva'));
        $grid->column('total_ht', __('Total ht'));
        $grid->column('total_ttc', __('Total ttc'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        $grid->actions(function ($actions) {
            $actions->add(new ViewPurchaseOrder);
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(PurchaseOrder::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('date', __('Date'))->as(function ($date) { return date("d/m/Y", strtotime($date)); });
        $show->field('document', __('Document'));
        if($all===1)
        $show->field('client', __('Clients id'))->as(function($client){
            return $client->full_name;
        });
        $show->field('company', __('companies_id'))->as(function($companies_id){
            return $company::find($companies_id)->company;
        });
        $show->field('paiementmodes_id', __('mode de paiement'))->display(function($paiementmodes_id) {
            return Paiementmode::find($paiementmodes_id)->name;
        });
        $show->field('devise', __('Devise'));
        $show->field('tva', __('Tva'))->as(function ($tva) { return ($tva*100)."%"; });
        $show->field('total_ht', __('Total ht'))->as(function ($totatl) { return number_format($totatl,2,"."," "); });
        $show->field('total_ttc', __('Total ttc'))->as(function ($totatl) { return number_format($totatl,2,"."," "); });
        // $show->field('created_at', __('Created at'))->as(function ($date) { return date("d/m/Y H:i:s", strtotime($date)); });
        // $show->field('updated_at', __('Updated at'))->as(function ($date) { return date("d/m/Y H:i:s", strtotime($date)); });
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new PurchaseOrder());
        $form->date('date', __('Date'))->default(date('Y-m-d'));
        $form->text('name', __('Name'))->default(PurchaseOrder::newBonCommande());
        $form->file('document', __('Document'));
         if (\Request::is('admin/client/purchase-orders/create')) { 
            $form->select('companies_id', __('Company'))->options(\App\Company::where('contre_partie', 'company_interne')->pluck('company', 'id'));
            $form->select('clients_id', __('Client'))->options(\App\Models\Client::all()->pluck('full_name', 'id'));
        }
        if(\Request::is('admin/fournisseur/devis/create')){
            $form->select('companies_id', __('Company'))->options(\App\Company::where('contre_partie', 'fournisseur')->pluck('company', 'id'));
            $form->text('clients_id', __('Client'))->default(PurchaseOrder::generate_random());
        }
        
        $form->select('paiementmodes_id', __('Mode de paiement'))->options(Paiementmode::all()->pluck('name', 'id'));
        $form->text('devise', __('Devise'))->default("MAD");
        $form->decimal('tva', __('Tva'))->default(0.2);
        $form->decimal('total_ht', __('Total ht'));
        $form->saved(function (Form $form) {
            $purchaseOrder = $form->model();
            $purchaseOrder->total_ttc = $purchaseOrder['total_ht'] * (1 + $purchaseOrder->tva);

            if (\Request::is('admin/client/purchase-orders')) { 
                $purchaseOrder->contre_partie='client';
            }
            if(\Request::is('admin/fournisseur/devis')){
                $purchaseOrder->contre_partie='fournisseur';
            }
            $purchaseOrder->save();
        });

        return $form;
    }

    public function view($id)
    {
        $file_path = PurchaseOrder::find($id);
        $file= $file_path->document;
        return view('purchaseorder.preview', compact('file'));
    }
    public function view_facture_free($id)
    {
        $file_path = PurchaseOrder::find($id);
        $file= $file_path->document;
        return view('purchaseorder.preview', compact('file'));
    }
}