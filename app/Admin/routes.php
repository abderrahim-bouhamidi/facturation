<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    //tableau de 
    //preview client
    $router->get('clients/{id}/view-invoice-client', 'ClientController@view_invoice');
    $router->get('clients/{id}/view-quotation-client', 'ClientController@view_quote');
    $router->get('clients/{id}/view-proposal-client', 'ClientController@view_proposal');

    // bon de commande client
    $router->get('client/purchase-orders/{id}/view', 'PurchaseOrderController@view');
    $router->resource('client/purchase-orders', PurchaseOrderController::class);
    // devis client
    $router->get('client/devis/{id}/print', 'DevisController@print');
    $router->get('client/devis/{id}/view-bc', 'DevisController@view_bc');
    $router->get('client/fournisseur/devis/{id}/print', 'DevisController@print');
    $router->get('client/fournisseur/devis/{id}/view-bc', 'DevisController@view_bc');
    $router->resource('client/devis', DevisController::class);
    // facture client
    $router->get('client/invoices/{id}/print', 'InvoiceController@print');
    $router->get('client/invoices/{id}/view-invoice-bc', 'InvoiceController@preview');
    $router->get('client/invoices/{id}/view-devis', 'InvoiceController@preview_devis');
    $router->resource('client/invoices', InvoiceController::class);
    
    // bon de commande fournisseur
    $router->get('fournisseur/purchase-orders/{id}/view', 'PurchaseOrderController@view');
    $router->resource('fournisseur/purchase-orders', DevisController::class);
    $router->get('fournisseur/devis/{id}/view', 'PurchaseOrderController@view_facture_free');
    // Devis fournisseur
    $router->get('fournisseur/devis/{id}/print', 'DevisController@print');
    $router->get('fournisseur/devis/{id}/view-bc', 'DevisController@view_bc');
    $router->get('fournisseur/fournisseur/devis/{id}/print', 'DevisController@print');
    $router->get('fournisseur/fournisseur/devis/{id}/view-bc', 'DevisController@view_bc');
    $router->resource('fournisseur/devis', PurchaseOrderController::class);
    // facture fournisseur
    $router->get('fournisseur/invoices/{id}/print', 'InvoiceController@print');
    $router->get('fournisseur/invoices/{id}/view-invoice-bc', 'InvoiceController@preview');
    $router->get('fournisseur/invoices/{id}/view-devis', 'InvoiceController@preview_devis');
    $router->get('fournisseur/invoices/{id}/view-facture', 'InvoiceController@view_facture');

    $router->resource('fournisseur/invoices', InvoiceController::class);
    
    $router->get('fournisseur/purchase-orders/{id}/print-it', 'DevisController@printer');

    
    $router->resource('clients', ClientController::class);
    $router->resource('fournisseur/fournisseur', CompanyController::class);

    $router->resource('paiementmodes', PaiementmodeController::class);
    $router->resource('companies', CompanyController::class);
    $router->resource('projects', ProjectController::class);
});
