<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Company;
use App\Models\Devis;
use App\PurchaseOrder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Invoice extends Model
{
    protected $title = 'invoices';

    use SoftDeletes, Notifiable;

    public function client()
    {
        return $this->belongsTo(Client::class,'clients_id');
    }
    public function devis()
    {
        return $this->belongsTo(Devis::class,'devis_id');
    }
    public function PurchaseOrder()
    {
        return $this->belongsTo(PurchaseOrder::class,'purchase_orders_id');
    }
    public function company()
    {
        return $this->belongsTo(Company::class,'companies_id');
    }

    public function paiementmode()
    {
        return $this->belongsTo(Paiementmode::class,'paiementmodes_id');
    }

    public function lines()
    {
        return $this->hasMany(Invoicelines::class,'invoices_id');
    }

    public static function newReference()
    {
        $last = Self::where("date","like",Date("Y")."%")->orderBy("id","desc")->first();
        if($last)
        {
            $parts = explode("-", $last->name);
            $num = (int)$parts[1];
            $num ++;
        }
        else
            $num = 1;

        return date("Y").'-'.str_pad($num, 3, "0", STR_PAD_LEFT);
    }
     public static function newInvoice()
    {
        $invoice = new Invoice();
        $lastOne = Self::orderBy("created_at","DESC")->first();
        
        $invoice['name'] = "F".'-'.str_pad($lastOne!=null?$lastOne->id+1:1, 6, "0", STR_PAD_LEFT);    
        return $invoice['name'];
    }
    public static function boot() {
        parent::boot();
        static::created(function($client) {
        });
        static::updated(function($client) {
        });
        static::deleting(function($client) {
            $this->client()->delete();
            $this->PurchaseOrder()->delete();
            $this->devis()->delete();
            $this->lines()->delete();
        });
    }

}