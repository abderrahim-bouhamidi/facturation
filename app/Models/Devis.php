<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Company;
use App\PurchaseOrder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Devis extends Model
{
    use SoftDeletes, Notifiable;
    public function client()
    {
        return $this->belongsTo(Client::class,'clients_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class,'companies_id');
    }
    public function PurchaseOrder()
    {
        return $this->belongsTo(PurchaseOrder::class,'purchase_orders_id');
    }
    public function paiementmode()
    {
        return $this->belongsTo(Paiementmode::class,'paiementmodes_id');
    }

    public function lines()
    {
        return $this->hasMany(Devislines::class,'devis_id');
    }

    public static function newReference()
    {
        $last = Self::where("date","like",Date("Y")."%")->orderBy("id","desc")->first();
        if($last)
        {
            $parts = explode("-", $last->reference);
            $num = (int)$parts[1];
            $num ++;
        }
        else
            $num = 1;

        return date("Y").'-'.str_pad($num, 3, "0", STR_PAD_LEFT);
    }
    public static function newDevis()
    {
        $Devis = new Devis();
            $lastOne = Self::orderBy("created_at","DESC")->first();
            $Devis->name = "D".'-'.str_pad($lastOne!=null?$lastOne->id+1:1, 6, "0", STR_PAD_LEFT);    
            return $Devis->name;
    }
    public static function boot() {
        parent::boot();
        static::created(function($client) {
        });
        static::updated(function($client) {
        });
        static::deleting(function($client) {
            $this->client()->delete();
            $this->PurchaseOrder()->delete();
            $this->lines()->delete();
        });
    }
}