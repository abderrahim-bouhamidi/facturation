<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
class Invoicelines extends Model
{
    use SoftDeletes, Notifiable;

    public function invoice()
    {
        return $this->belongsTo(Invoice::class,'invoices_id');
    }
}
