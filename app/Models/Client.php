<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Invoice;
use App\Models\Devis;
use App\PurchaseOrder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
class Client extends Model
{
    use SoftDeletes, Notifiable;

    public function devis()
    {
        return $this->belongsTo(Devis::class,'devis_id');
    }
    public function PurchaseOrder()
    {
        return $this->belongsTo(PurchaseOrder::class,'purchase_orders_id');
    }    
    public function Invoice()
    {
        return $this->belongsTo(Invoice::class,'invoices_id');
    }
    public function getFullNameAttribute()
    {
        return $this->contact_name . ' | ' . $this->company;
    }
    public static function boot() {
        parent::boot();
        static::created(function($client) {

        });
        static::updated(function($client) {
        });
        static::deleting(function($client) {
            $client->PurchaseOrder()->delete();
            $client->devis()->delete();
            $client->Invoice()->delete();
        });
    }
    
}
