<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
class Paiementmode extends Model
    {
    use SoftDeletes, Notifiable;
    protected $fillable = [
        'name'
    ];
}
