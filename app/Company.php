<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\PurchaseOrder;
use Illuminate\Database\Eloquent\SoftDeletes;
class Company extends Model
{
    use SoftDeletes, Notifiable;
     protected $fillable = [
        'logo', 'address', 'company', 'email', 'website', 'ICE', 'FISC', 'RC','tampon'
    ];

}