<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->text('logo')->nullable();
            $table->text('address')->nullable();
            $table->string('company');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('website')->nullable();
            $table->string('ICE')->nullable();
            $table->string('FISC')->nullable();
            $table->string('RC')->nullable();
            $table->string('TP')->nullable();
            $table->string('CNSS')->nullable();
            $table->string('RIB')->nullable();
            $table->text('tampon')->nullable();
            $table->string('contre_partie')->nullable();
            $table->timestamps();

            $table->softDeletes();       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
