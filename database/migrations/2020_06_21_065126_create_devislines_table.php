<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevislinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devislines', function (Blueprint $table) {
            $table->id();
            $table->integer('devis_id');
            $table->string('reference');
            $table->text('description')->nullable();
            $table->integer('qty');
            $table->float('price_uht');
            $table->timestamps();

            $table->softDeletes();       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devislines');
    }
}
