<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->date('date');
            $table->string('reference');
            $table->integer('clients_id');
            $table->integer('companies_id');
            $table->integer('paiementmodes_id');
            $table->integer('devis_id');
            $table->integer('purchase_orders_id');
            $table->string('devise')->default("MAD");
            $table->float('tva')->nullable();
            $table->float('total_ht')->nullable();
            $table->float('total_ttc')->nullable();
            $table->integer('payement_statuts')->nullable();
            $table->date('due_date');
            $table->string('contre_partie')->nullable();
            $table->string('document')->nullable();
            $table->timestamps();       
            $table->softDeletes();      
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
