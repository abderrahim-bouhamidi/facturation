<?php
use Faker\Generator as Faker;


$factory->define(\App\Company::class, function (Faker $faker) {
    return [
            'logo'=> $this->faker->image('public/assets/images/company_logo', 400, 300, null, false),
            'address'=> $this->faker->streetAddress,
            'company' => $this->faker->company,
            'email' => $this->faker->companyEmail,
            'website' => $this->faker->url,
            'ICE' => $this->faker->swiftBicNumber,
            'FISC' => $this->faker->swiftBicNumber,
            'RC' => $this->faker->swiftBicNumber,
            'tampon'=> $this->faker->image('public/assets/images/tampons', 400, 300, null, false),
        ];
});
