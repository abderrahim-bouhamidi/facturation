<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = (int)$this->command->ask('How many Company do you need ?', 5);

        $this->command->info("Creating {$count} companies.");

        //Create the Company
        factory(App\Company::class, $count)->create()->each(function ($company) {
        $company->posts()->save(factory(App\Company::class)->make());
    });
    }
}
