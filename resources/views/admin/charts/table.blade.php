<?php
  $year = [];
  $mois = [];
  $income = [];
  $outcome = [];
  $yearArray = json_decode($click, true); 
  foreach ($yearArray as $row) {
    $year[] = $row['year'];
    $income_y[] = $row['total_ht'];
    $outcome_y[] = $row['total_ttc'];
  }
?>
<!DOCTYPE html>
<html>
<head>
<style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #04AA6D;
  color: white;
}
</style>
</head>
<body>

<table id="customers">
  <tr>
    <th>Date</th>
    <th>Revenue</th>
    <th>charge</th>
    <th>Gain</th>
  </tr>
  @for($i=0;$i<count($year);$i++)
                    <tr>
                      <td>
                          {{ $year[$i]}}
                      </td>                     
                    <td>
                        {{$income_y[$i]}}
                    </td>
                    <td>
                        {{$outcome_y[$i]}}
                    </td>
                    <td>
                      {{$income_y[$i] - $outcome_y[$i]}}
                    </td>
                  </tr> 
    @endfor 
</table>

</body>
</html>
