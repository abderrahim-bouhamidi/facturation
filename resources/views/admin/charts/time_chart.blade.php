<?php
  // $year = [];
  // $mois = [];
  // $income = [];
  // $outcome = [];

  // $responseArray = json_decode($viewer, true); 
  // $yearArray = json_decode($click, true); 
  // foreach ($yearArray as $row) {  
  //   $year[] = $row['year'];
  //   $income_y[] = $row['income'];
  //   $outcome_y[] = $row['outcome'];
  // }
  // foreach ($responseArray as $row) 
  //   {
  //   $date[]= $row['date'];
  //   $mois[] = $row['month'];
  //   $income[] = $row['income'];
  //   $outcome[] = $row['outcome'];

  ?>

<canvas id="myChart" width="100" height="50" style='height:100px!important;'></canvas>
<script>
$(function () {
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [    
              @foreach($viewer as $key => $elem)
                    '{{$viewer[$key]->month}}',
              @endforeach 
            ],

            datasets: [{  
                label: 'INCOME',  
                backgroundColor: "rgba(96,137,5,0.5)",  
                data: [
                  @foreach($viewer as $key => $elem)
                        {{$viewer[$key]->income}},
                  @endforeach 

                ]  
               }, {  
                label: 'OUTCOME',  
                backgroundColor: "rgba(150,16,25,2)",  
                data: [
                  @foreach($viewer as $key => $elem)
                    @if($viewer[$key]==$viewer[$key+1]){
                        
                    }
                    @else{

                    }
                        '{{$viewer[$key]->outcome}}',
                  @endforeach 

                ]    
            }]  
       },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
});
</script>
<!DOCTYPE html>
<html>
<head>
<style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #04AA6D;
  color: white;
}
</style>
</head>
<body>

<table id="customers">
  <tr>
    <th>Date</th>
    <th>Revenue</th>
    <th>charge</th>
    <th>Gain</th>
  </tr>
                @foreach($viewer as $key => $elem)
                     <tr>
                      <td>
                          {{$viewer[$key]->month}}
                      </td>                     
                    <td>
                        {{$viewer[$key]->income}} DH
                    </td>
                    <td>
                        {{$viewer[$key]->outcome}} DH
                    </td>
                    <td>
                      {{$viewer[$key]->income - $viewer[$key]->outcome}} DH
                    </td>
                  </tr> 
              @endforeach 
</table>

</body>
</html>