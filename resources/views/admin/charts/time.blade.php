<?php
  $year = [];
  $mois = [];
  $income = [];
  $outcome = [];

  $responseArray = json_decode($viewer, true); 
  $yearArray = json_decode($click, true); 
  foreach ($yearArray as $row) {
    $year[] = $row['year'];
    $income_y[] = $row['income'];
    $outcome_y[] = $row['outcome'];
  }
  ?>

<canvas id="chartyear" width="100" height="50" style='height:100px!important;'></canvas>
<script>
$(function () {
    var ctx = document.getElementById("chartyear").getContext('2d');
    var chartyear = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [    
              @for($i=0;$i<count($year);$i++)
                    '{{ $year[$i]}}',
              @endfor 
            ],

            datasets: [{  
                label: 'INCOME',  
                backgroundColor: "rgba(96,137,5,0.5)",  
                data: [
                  @for($i=0;$i<count($income_y);$i++)
                    {{$income_y[$i]}},
                  @endfor 

                ]  
               }, {  
                label: 'OUTCOME',  
                backgroundColor: "rgba(150,16,25,2)",  
                data: [
                  @for($i=0;$i<count($outcome_y);$i++)
                    {{$outcome_y[$i]}},
                  @endfor 

                ]    
            }]  
       },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
});
</script>
<!DOCTYPE html>
<html>
<head>
<style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #04AA6D;
  color: white;
}
</style>
</head>
<body>

<table id="customers">
  <tr>
    <th>Date</th>
    <th>Revenue</th>
    <th>charge</th>
    <th>Gain</th>
  </tr>
  @for($i=0;$i<count($year);$i++)
                    <tr>
                      <td>
                          {{ $year[$i]}}
                      </td>                     
                    <td>
                        {{$income_y[$i]}}
                    </td>
                    <td>
                        {{$outcome_y[$i]}}
                    </td>
                    <td>
                      {{$income_y[$i] - $outcome_y[$i]}}
                    </td>
                  </tr> 
    @endfor 
</table>

</body>
</html>